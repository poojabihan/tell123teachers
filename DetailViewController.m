//
//  DetailViewController.m
//  ChangeItApp
//
//  Created by Tarun Sharma on 01/04/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "DetailViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "ImageViewController.h"
#import "constant.h"
#import "UIImageView+WebCache.h"

@interface DetailViewController ()
{
    MBProgressHUD * hud;
    
}
@end

@implementation DetailViewController



#pragma mark ViewLifeCycle Delegates

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"Details"];

    self.imageVw.clipsToBounds = YES;
    
    self.imageVw.contentMode = UIViewContentModeScaleAspectFit;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![_imageName isEqualToString:@"noImage"])
        {
            //imageVw.image=[UIImage imageWithData:_imageData];
            //NSString * imageString=[[NSString stringWithFormat:@"%@%@",kImageURL, _imageName]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSString * imageString = [[NSString stringWithFormat:@"%@%@",kImageURL, _imageName]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageString].absoluteString];
            if(image == nil)
            {
                [self.imageVw sd_setImageWithURL:[NSURL URLWithString:imageString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [self.imageVw setImage:image];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [self.imageVw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        //[activityIndicator stopAnimating];
                    }
                }];
            } else {
                [self.imageVw setImage:image];
                //[activityIndicator removeFromSuperview];
            }
            
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(singleTapGestureCaptured)];
            [self.imageVw addGestureRecognizer:singleTap];
            [self.imageVw setMultipleTouchEnabled:YES];
            [self.imageVw setUserInteractionEnabled:YES];
            
            
        }
        else{
            self.imageVw.image=[UIImage imageNamed:@"ImgNotFound.png"];
        }
        
        self.dateAndTime.text = self.date;
        self.detailText.text = self.details;
        self.locationLabel.text=self.locationStr;
        
    });

//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    if(internetStatus != NotReachable)
//    {
//    NSURLSession *session=[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    
//    [indicator setTransform:CGAffineTransformMakeScale(1.5f, 1.5f)];
//    [indicator setColor:[UIColor colorWithRed:45/255.0f green:133/255.0f blue:202/255.0f alpha:1]];
//    
//    [indicator startAnimating];
//    
//    [[session dataTaskWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageStr]] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//      {
//          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//              
//              imageVw.image  = [UIImage imageWithData:data];
//              [indicator stopAnimating];
//              
//          }];
//      }] resume];
//
//    dateAndTime.text = date;
//    detailText.text = details;
//    locationLabel.text=location;
//    
//    }
//    else
//    {
//        UIAlertView *myAlert = [[UIAlertView alloc]
//                                initWithTitle:@"Tell 123Teachers"
//                                message:@"Internet Connection required."
//                                delegate:self
//                                cancelButtonTitle:nil
//                                otherButtonTitles:@"Ok",nil];
//        [myAlert show];
//    }
//
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)singleTapGestureCaptured{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    //hud.dimBackground = YES;
    
    ImageViewController *image = [self.storyboard instantiateViewControllerWithIdentifier:@"image"];
    image.imageVariable=self.imageVw.image;
    [self.navigationController pushViewController:image animated:YES];
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
    
    
}
#pragma mark Gesture Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}




@end
