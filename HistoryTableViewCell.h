//
//  HistoryTableViewCell.h
//  ChangeItApp
//
//  Created by Tarun Sharma on 26/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imagevw;

@property (weak, nonatomic) IBOutlet UILabel *textOfLocation;

@property (weak, nonatomic) IBOutlet UILabel *dateAndTime;

@property (weak, nonatomic) IBOutlet UILabel *dataText;

@property (weak, nonatomic) IBOutlet UIButton *myButton;

@property (weak, nonatomic) IBOutlet UIButton *sendQueryButton;

@end
