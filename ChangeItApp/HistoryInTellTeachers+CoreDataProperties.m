//
//  HistoryInTellTeachers+CoreDataProperties.m
//  Tell123Teachers
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryInTellTeachers+CoreDataProperties.h"

@implementation HistoryInTellTeachers (CoreDataProperties)

+ (NSFetchRequest<HistoryInTellTeachers *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"HistoryInTellTeachers"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic image;
@dynamic locationdetail;
@dynamic msgdetail;

@end
