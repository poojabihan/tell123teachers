//
//  MessagesViewController.h
//  Tell123Teachers
//
//  Created by Tarun Sharma on 02/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;
-(void)loadingElementsInMessage;

@end
