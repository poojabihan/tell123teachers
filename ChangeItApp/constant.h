//
//  constant.h
//  Tell123Teachers
//
//  Created by Tarun Sharma on 04/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#ifndef constant_h
#define constant_h
#define UIAppDelegate \
((AppDelegate*)[[UIApplication sharedApplication]delegate])
//constants for Webservice
//demo_tellsid
//tellsid

#define kBaseURL @"http://live.thechangeconsultancy.co/tellsid/index.php/apiteachers/"
#define kImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/123teachers/"
#define kChatImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/chatimg/"

//#define kBaseURL @"http://tellsid.softintelligence.co.uk/index.php/apiteachers/"
//#define kImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/123teachers/"
//#define kChatImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/chatimg/"


#define kAppNameAPI @"123Teachers"
#define khudColour [UIColor colorWithRed:51/255.0f green:103/255.0f blue:153/255.0f alpha:1]
#define kAppNameAlert @"Tell 123Teachers"
#define kInternetOff @"Internet Connection Required!"
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#endif /* constant_h */
