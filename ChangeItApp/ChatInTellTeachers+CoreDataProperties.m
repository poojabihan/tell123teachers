//
//  ChatInTellTeachers+CoreDataProperties.m
//  Tell123Teachers
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ChatInTellTeachers+CoreDataProperties.h"

@implementation ChatInTellTeachers (CoreDataProperties)

+ (NSFetchRequest<ChatInTellTeachers *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ChatInTellTeachers"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic message;
@dynamic usertype;
@dynamic posttype;

@end
