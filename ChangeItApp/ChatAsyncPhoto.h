//
//  ChatAsyncPhoto.h
//  Tell123Teachers
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import "JSQPhotoMediaItem.h"

@interface ChatAsyncPhoto : JSQPhotoMediaItem
@property (nonatomic, strong) UIImageView *asyncImageView;

- (instancetype)initWithURL:(NSURL *)URL;

@end
