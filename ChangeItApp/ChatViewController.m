//
//  ChatViewController.m
//  Tell123Teachers
//
//  Created by Tarun Sharma on 02/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ChatViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "MessagesViewController.h"
#import "ChatInTellTeachers+CoreDataProperties.h"
#import "constant.h"
#import "ChatImageViewController.h"
#import "ChatAsyncPhoto.h"

@interface ChatViewController ()
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubble;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubble;
@property (strong, nonatomic) JSQMessagesAvatarImage *incomingAvatar;
@property (strong, nonatomic) JSQMessagesAvatarImage *outgoingAvatar;
@property (strong, nonatomic) NSMutableArray * dictArray,*temp;
@property (atomic, assign) BOOL canceled;

@end
NSManagedObjectContext *chatContext;

@implementation ChatViewController

{
    NSDictionary * countDictionary;
    NSString * responseStr;
    NSTimer* timer;
    NSArray *resultArray;
    MBProgressHUD *hud ;
    UIImage *chosenImage;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"change it id view did load %@",self.itemID);
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"Chat"];
    [self.inputToolbar.contentView.textView setKeyboardType:UIKeyboardTypeASCIICapable];
    
    
    
    //self.inputToolbar.contentView.leftBarButtonItem = nil;
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    chatContext=[UIAppDelegate managedObjectContext];
    
    // ① Own senderId, senderDisplayName The setting
    
    self.senderId = @"User";
    self.senderDisplayName = @"User";
    // ② MessageBubble (Balloon background) The setting
    JSQMessagesBubbleImageFactory *bubbleFactory = [JSQMessagesBubbleImageFactory new];
    //self.incomingBubble = [bubbleFactory  incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    //self.incomingBubble=[bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1]];
    self.incomingBubble=[bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    //self.outgoingBubble = [bubbleFactory  outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
    //self.outgoingBubble = [bubbleFactory  outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:39/255.0f green:115/255.0f blue:164/255.0f alpha:1]];
    //self.outgoingBubble = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    self.outgoingBubble = [bubbleFactory outgoingMessagesBubbleImageWithColor:khudColour];
    
    //self.incomingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"TD" backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f] textColor:[UIColor colorWithWhite:0.60f alpha:1.0f] font:[UIFont systemFontOfSize:14.0f] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    //self.outgoingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"TD" backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f] textColor:[UIColor colorWithWhite:0.60f alpha:1.0f] font:[UIFont systemFontOfSize:14.0f] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    // ③ Set an avatar image
    //self.incomingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"User2"] diameter:64];
    //self.outgoingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"1.jpg"] diameter:64];
    // ④ Initialize an array of message data
    self.messages = [NSMutableArray array];
    
    if (UIAppDelegate._isCurrentpageChat==YES) {
        
        
        
        
        //
        //        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 1, 20, 20)];
        //
        //                // [backButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
        //        [backButton setShowsTouchWhenHighlighted:TRUE];
        //        [backButton addTarget:self action:@selector(onClickBackPushButton) forControlEvents:UIControlEventTouchUpInside];
        //        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        //        self.navigationItem.hidesBackButton = TRUE;
        //        self.navigationItem.leftBarButtonItem = barBackItem;
        //        [barBackItem setTitle:@"back"];
        //self.itemID=@"702";
        
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Go back"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(onClickBackPushButton)];
        [self.navigationItem setLeftBarButtonItem:item animated:YES];
        UIAppDelegate._isCurrentpageChat=NO;
        [self loadEarlierMessages];
        
    }
    else{
        [self loadEarlierMessages];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)onClickBackPushButton{
    
    //    [self.navigationController popViewControllerAnimated:YES];
    //InboxViewController * in=[[InboxViewController alloc]init];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
//-(void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender{
//    JSQMessage *newMessage = [[JSQMessage alloc]initWithSenderId:@"" senderDisplayName:@"" date:[NSDate date] text:@"It Works (?)"];
//
//    [self.messages insertObject:newMessage atIndex:0];
//
//    [self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
//    [self.collectionView reloadData];
//
//}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:(BOOL)animated];
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:(BOOL)animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        timer = [NSTimer scheduledTimerWithTimeInterval:5 target: self selector: @selector(loadEarlierMessages) userInfo:nil repeats: YES];
    }
//    else{
//        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        //hud.contentColor =[UIColor colorWithRed:39/255.0f green:115/255.0f blue:164/255.0f alpha:1];
//        hud.contentColor =[UIColor redColor];
//        // Set the text mode to show only text.
//        hud.mode = MBProgressHUDModeText;
//        hud.label.text = NSLocalizedString(@"Internet Connection Required!", @"HUD message title");
//        // Move to bottm center.
//        hud.offset = CGPointMake(1.f, MBProgressMaxOffset);
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [hud hideAnimated:YES afterDelay:0.5f];
//        });
//        
//    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [timer invalidate];
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}


#pragma mark GetMessages Method

-(void)loadEarlierMessages
{
    NSLog(@"item id in method with string %@",self.itemID);
    self.temp = [NSMutableArray array];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //Do background work
            self.dictArray = [[NSMutableArray alloc]init];
            
            
            
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            // getting an NSString
            NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
            
            NSString *str = [NSString stringWithFormat:@"getusermessages?changeit_id=%@&email_id=%@&app_name=%@",self.itemID,emailIdString,kAppNameAPI];
            
            NSURL *baseURL = [NSURL URLWithString:kBaseURL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"Response : %@",responseObject);
                 if ([responseObject objectForKey:@"response"]) {
                     resultArray = [responseObject objectForKey:@"response"];
                     
                     NSLog (@"Number of elements in array = %lu", (unsigned long)[resultArray count]);
                     [self.temp removeAllObjects];
                     for (NSDictionary* msg in resultArray) {
                         
                         [self.temp addObject:[self JSQMessageFromData:msg]];
                     }
                     
                     
                     if (self.messages.count<self.temp.count) {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             // Update UI here
                             //[self.messages removeAllObjects];
                             NSLog(@"self.messages count %lu and temp count %lu",(unsigned long)self.messages.count,(unsigned long)self.temp.count);
                             self.messages=self.temp;
                             [self.collectionView reloadData];
                             [self finishReceivingMessageAnimated:YES];
                             
                             
                         });
                         
                     }
                 }
                 else{
                     
                 }
               

                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"Error : %@",error);
                
            }];

            
            
            
        });
        
    }
    else{
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ChatInTellTeachers" inManagedObjectContext:chatContext];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"changeitid == %@", _itemID]];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:YES];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        
        resultArray = [chatContext executeFetchRequest:fetchRequest error:&error];
        NSLog(@"Count in coredata %lu",(unsigned long)resultArray.count);
        //[self.temp removeAllObjects];
        for (ChatInTellTeachers * chatInfo in resultArray) {
            
            [self.temp addObject:[self JSQMessageFromDataOffline:chatInfo]];
        }
        self.messages=self.temp;
        dispatch_async (dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
        });
        [self finishReceivingMessageAnimated:YES];
        
        
    }
    
}

- (JSQMessage *) JSQMessageFromData:(NSDictionary*)msg {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //[dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date =[dateFormat dateFromString:msg[@"created_date"]];
    
    
    //return [[JSQMessage alloc]initWithSenderId:[msg objectForKey:@"user_type"] senderDisplayName:[msg objectForKey:@"user_type"] date:date text:[msg objectForKey:@"message"]];
    
    if ([[msg objectForKey:@"post_type"] isEqualToString:@"msg"]) {
        return [[JSQMessage alloc]initWithSenderId:[msg objectForKey:@"user_type"] senderDisplayName:[msg objectForKey:@"user_type"] date:date text:[msg objectForKey:@"message"]];
    }
    else{
        //NSString * imageString=[[NSString stringWithFormat:@"%@%@",kChatImageURL, [msg objectForKey:@"message"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString * imageString= [[NSString stringWithFormat:@"%@%@",kChatImageURL, [msg objectForKey:@"message"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        ChatAsyncPhoto *photoItem = [[ChatAsyncPhoto alloc] initWithURL:[NSURL URLWithString:imageString]];
        return [[JSQMessage alloc] initWithSenderId:[msg objectForKey:@"user_type"] senderDisplayName:[msg objectForKey:@"user_type"] date:date media:photoItem];
        
        
    }

    
    
}
- (JSQMessage *) JSQMessageFromDataOffline:(ChatInTellTeachers *)chatInfo {
    NSLog(@"Dictionary Msg %@",chatInfo);
    //Chat * chatInfo=msg;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //[dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date =[dateFormat dateFromString:chatInfo.createddate];
    
    
    //return [[JSQMessage alloc]initWithSenderId:chatInfo.usertype senderDisplayName:chatInfo.usertype date:date text:chatInfo.message];
    if ([[chatInfo valueForKey:@"posttype"] isEqualToString:@"msg"]) {
        return [[JSQMessage alloc]initWithSenderId:[chatInfo valueForKey:@"usertype"] senderDisplayName:[chatInfo valueForKey:@"usertype"] date:date text:[chatInfo valueForKey:@"message"]];
    }
    else{
        
        //NSString * imageString=[[NSString stringWithFormat:@"%@%@",kChatImageURL, [chatInfo valueForKey:@"message"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString * imageString= [[NSString stringWithFormat:@"%@%@",kChatImageURL, [chatInfo valueForKey:@"message"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        ChatAsyncPhoto * chatAsyncPhotoItem = [[ChatAsyncPhoto alloc] initWithURL:[NSURL URLWithString:imageString]];
        return [[JSQMessage alloc] initWithSenderId:[chatInfo valueForKey:@"usertype"]senderDisplayName:[chatInfo valueForKey:@"usertype"] date:date media:chatAsyncPhotoItem];
        
        
    }

    
    
}
- (NSString *) getImageFromCoreData:(NSManagedObject *)chatInfo {
    NSLog(@"Dictionary Msg %@",chatInfo);
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // NSDate *date =[dateFormat dateFromString:chatInfo.createddate];
    
    
    
    NSString * imageString=[chatInfo valueForKey:@"message"];
    //[NSString stringWithFormat:@"%@%@",kChatImageURL, chatInfo.message];
    
    //ChatAsyncPhoto * chatAsyncPhotoItem = [[ChatAsyncPhoto alloc] initWithURL:[NSURL URLWithString:imageString]];
    return imageString;
    
}


//-(void)hello{
//    // dispatch_async(dispatch_get_main_queue(), ^{
//    JSQMessage * message = [JSQMessage messageWithSenderId:@"Admin"
//                                               displayName:@"AdminName"
//                                                      text:@"From View Did Appear...."];
//    [self.messages addObject:message];
//    [self.collectionView reloadData];
//    [self finishReceivingMessageAnimated:YES];
//    //});
//    
//}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - JSQMessagesViewController

// ⑤ Send Called when the button is pressed
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        //[timer invalidate];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id %@",emailIdString);
        
        //NSString * post=[NSString stringWithFormat:@"changeit_id=%@&app_name=%@&device_id=%@&email_id=%@&message=%@&post_type=%@",self.itemID,kAppNameAPI,[[[UIDevice currentDevice] identifierForVendor] UUIDString],emailIdString,text,@"msg"];
        
        NSDictionary *dictParam = @{@"changeit_id":self.itemID,@"app_name":kAppNameAPI,@"device_id":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"email_id":emailIdString,@"message":text,@"post_type":@"msg"};

        self.URLString=[NSString stringWithFormat:@"%@sendchatmsg/",kBaseURL];
        [Webservice requestPostUrl:self.URLString parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"Response : %@",response);
            if (response==NULL) {
                [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    //[dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                    NSDate *date =[dateFormat dateFromString:response[@"created_date"]];
                    [JSQSystemSoundPlayer jsq_playMessageSentSound];
                    
                    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:[response objectForKey:@"user_type"] senderDisplayName:[response objectForKey:@"user_type"] date:date  text:[response objectForKey:@"message"]];
                    //[message initWithSenderId:@"user1" senderDisplayName:@"classmethod" date:[NSDate date] text:@"test"];
                    
                    //JSQMessage *message = [JSQMessage messageWithSenderId:displayName:senderDisplayName text:text];
                    NSLog(@"Message %@",message);
                    [self.messages addObject:message];
                    [self finishSendingMessageAnimated:YES];
                    //[self receiveAutoMessage];
                });
 
            }
            
            
        } failure:^(NSError *error) {
            NSLog(@"Error : %@",error);
           [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];

            
        }];
        
        
    }
    else
    {
       [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
    }
    
}

#pragma mark - JSQMessagesCollectionViewDataSource
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
    
}
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    //if (indexPath.item % 3 == 0) {
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
    //}
    
    //return 0.0f;
}
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}


-(NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    
    //if (indexPath.item % 3 == 0) {
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    //}
    
    //return nil;
}
// ④Return a message data that refer to each item
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messages objectAtIndex:indexPath.item];
}

// ② Of each item MessageBubble (background) return it
- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubble;
    }
    return self.incomingBubble;
}

// ③ Return the avatar image of each item
- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingAvatar;
    }
    return self.incomingAvatar;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
    
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor whiteColor];
        }
        else {
            cell.textView.textColor = [UIColor blackColor];
        }
    }

    
    return cell;
}
#pragma mark - UICollectionViewDataSource

// ④It returns the total number of items
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.messages.count;
}

#pragma mark - Auto Message

// ⑥To receive a reply message (Automatic)
//- (void)receiveAutoMessage
//{
//    // 1 Receive a message after the second
//    [NSTimer scheduledTimerWithTimeInterval:1
//                                     target:self
//                                   selector:@selector(didFinishMessageTimer:)
//                                   userInfo:nil
//                                    repeats:NO];
//}
//
//- (void)didFinishMessageTimer:(NSTimer*)timer
//{
//    // Play sound effects
//    [JSQSystemSoundPlayer jsq_playMessageSentSound];
//    // To add a new message data
//    JSQMessage *message = [JSQMessage messageWithSenderId:@"Admin"
//                                              displayName:@"AdminName"
//                                                     text:@"Hello"];
//    [self.messages addObject:message];
//    //It completes the reception process of the message (Message is displayed on the screen)
//    [self finishReceivingMessageAnimated:YES];
//}

#pragma mark - sendingImage methods

- (void)didPressAccessoryButton:(UIButton *)sender
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        [self.inputToolbar.contentView.textView resignFirstResponder];
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self.inputToolbar.contentView.textView becomeFirstResponder];
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                
                [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
            }
            else
            {
                
                [self errorAlertWithTitle:@"Camera Not Found" message:@"This Device has no Camera" actionTitle:@"Dismiss"];
                
            }
            // Distructive button tapped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // OK button tapped.
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                
                UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
                photoPicker.delegate = self;
                photoPicker.allowsEditing = NO;
                photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                
                [self presentViewController:photoPicker animated:YES completion:NULL];
            }
            
            //        [self dismissViewControllerAnimated:YES completion:^{
            //        }];
        }]];
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];

    }
    else
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
        
    }
}

//- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == actionSheet.cancelButtonIndex) {
//        [self.inputToolbar.contentView.textView becomeFirstResponder];
//        return;
//    }
//    
//    switch (buttonIndex) {
//        case 0:
//            //[self.demoData addPhotoMediaMessage];
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//            {
//                
//                [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
//            }
//            else
//            {
//                UIAlertView *alert2 = [[UIAlertView alloc]initWithTitle:@"Camera Not Found " message:@"The Device has No camera" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//                [alert2 show];
//            }
//            
//            break;
//            
//        case 1:
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
//            {
//                
//                UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
//                photoPicker.delegate = self;
//                photoPicker.allowsEditing = YES;
//                photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//                
//                [self presentViewController:photoPicker animated:YES completion:NULL];
//            }
//            
//            
//            break;
//            
//    }
//    
//    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
//    
//    //[self finishSendingMessageAnimated:YES];
//}


-(void)showcamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        // My own Helper class is used here to pop a dialog in one simple line.
        //[Helper popAlertMessageWithTitle:@"Error" alertText:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access."];
        
        
        
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"Dismiss"];
        
    }
    else
    {
        [self camDenied];
    }
    
}
-(void)popCamera{
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:cameraPicker animated:YES completion:nil];
    
}
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
            }
        }
        @catch (NSException *exception)
        {
            
        }
        
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    //NSString *alertText;
    //NSString *alertButton;
    
    //    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    //    if (canOpenSettings)
    //    {
    //        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings of this app.\n\n2. Turn the Camera on.";
    //
    //        alertButton = @"Go";
    //    }
    //    else
    //    {
    //        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
    //
    //        alertButton = @"OK";
    //    }
    //
    //    UIAlertView *alert = [[UIAlertView alloc]
    //                          initWithTitle:@"Error"
    //                          message:alertText
    //                          delegate:self
    //                          cancelButtonTitle:alertButton
    //                          otherButtonTitles:nil];
    //    alert.tag = 3491832;
    //    [alert show];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    //self.imageVw.image = chosenImage;
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(chosenImage, nil, nil, nil);
    }
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    // Set the determinate mode to show task progress.
    
    hud.mode = MBProgressHUDModeDeterminate;
    hud.label.text = NSLocalizedString(@"Sending...", @"HUD loading title");
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        // Do something useful in the background and update the HUD periodically.
        //[timer invalidate];
        [self doSomeWorkWithProgress];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id %@",emailIdString);
        NSData *imageData = UIImageJPEGRepresentation(chosenImage, 0.2);
        
        NSString * imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        //NSString * post=[NSString stringWithFormat:@"changeit_id=%@&app_name=%@&device_id=%@&email_id=%@&message=%@&post_type=%@",self.itemID,kAppNameAPI,[[[UIDevice currentDevice] identifierForVendor] UUIDString],emailIdString,imgString,@"img"];
        
        NSDictionary *dictParam = @{@"changeit_id":self.itemID,@"app_name":kAppNameAPI,@"device_id":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"email_id":emailIdString,@"message":imgString,@"post_type":@"img"};
        
        
        self.URLString=[NSString stringWithFormat:@"%@sendchatmsg/",kBaseURL];
        [Webservice requestPostUrl:self.URLString parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"Response : %@",response);
            if (response==NULL) {
                dispatch_async (dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES afterDelay:2.0f];
                    [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
                });
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    //[dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                    NSDate *date =[dateFormat dateFromString:response[@"created_date"]];
                    [JSQSystemSoundPlayer jsq_playMessageSentSound];
                    NSString * imageString=[NSString stringWithFormat:@"%@%@",kChatImageURL, [response objectForKey:@"message"]];
                    
                    
                    ChatAsyncPhoto *photoItem = [[ChatAsyncPhoto alloc] initWithURL:[NSURL URLWithString:imageString]];
                    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:[response objectForKey:@"user_type"] senderDisplayName:[response objectForKey:@"user_type"] date:date media:photoItem];
                    [self.messages addObject:message];
                    
                    
                    [hud hideAnimated:YES];
                    [self finishSendingMessageAnimated:YES];
                    
                    [self loadEarlierMessages];
                    
                    MBProgressHUD * MBHud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                    MBHud.contentColor=khudColour;
                    // Set the custom view mode to show any view.
                    MBHud.mode = MBProgressHUDModeCustomView;
                    // Set an image view with a checkmark.
                    UIImage *image = [[UIImage imageNamed:@"CheckMark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    MBHud.customView = [[UIImageView alloc] initWithImage:image];
                    // Looks a bit nicer if we make it square.
                    MBHud.square = YES;
                    // Optional label text.
                    MBHud.label.text = NSLocalizedString(@"Sent", @"HUD done title");
                    
                    [MBHud hideAnimated:YES afterDelay:1.5f];
                    
                    
                });
 
            }
            
            
        } failure:^(NSError *error) {
            NSLog(@"Error : %@",error);
            
            dispatch_async (dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES afterDelay:2.0f];
               [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
            });

            
        }];
        
       
    });
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    
}
- (void)doSomeWorkWithProgress {
    self.canceled = NO;
    // This just increases the progress indicator in a loop.
    float progress = 0.0f;
    while (progress < 1.0f) {
        if (self.canceled) break;
        progress += 0.01f;
        dispatch_async(dispatch_get_main_queue(), ^{
            // Instead we could have also passed a reference to the HUD
            // to the HUD to myProgressTask as a method parameter.
            [MBProgressHUD HUDForView:self.navigationController.view].progress = progress;
        });
        usleep(50000);
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


//- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"Tapped message bubble!");
//    //JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
//    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
//
//
//    if (msg.isMediaMessage) {
//        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        hud.contentColor =khudColour;
//
//
//        // Set the label text.
//        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
//
//        //hud.dimBackground = YES;
//
//        ChatImageViewController *imageVC = [self.storyboard instantiateViewControllerWithIdentifier:@"chatImage"];
//        imageVC.imageVariable=[UIPasteboard generalPasteboard].image;
//        [self.navigationController pushViewController:imageVC animated:YES];
//
//        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
//
//
//    }
//
//
//}


//- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
//                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
//
//
//    if (msg.isMediaMessage) {
//
//        return 50.0f;
//
//    }
//    else{
//        return 0.0f;
//    }
//
//
//}
- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.row];
    NSLog(@"data %@",[self.messages objectAtIndex:indexPath.row]);
    if (message.isMediaMessage) {
        NSLog(@"Tapped photo message bubble!");
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.contentColor =khudColour;
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        
        
        NSLog(@"data %@",[self.messages objectAtIndex:indexPath.row]);
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if(internetStatus != NotReachable)
        {
            
            dispatch_queue_t myqueue = dispatch_queue_create("queue", NULL);
            dispatch_async(myqueue, ^{
                
                //Whatever is happening in the fetchedData method will now happen in the background
                NSString * imageUrl =[[resultArray objectAtIndex:indexPath.row]objectForKey:@"message"];
                NSLog(@"imageUrl %@",imageUrl);
                //NSData * data=[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    
                    ChatImageViewController *imageVC = [storyboard instantiateViewControllerWithIdentifier:@"chatImage"];
                    imageVC.imageURL=imageUrl;
                    [self.navigationController pushViewController:imageVC animated:YES];                [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
            });
            
            
        }
        
        else{
            dispatch_queue_t myqueue = dispatch_queue_create("queue", NULL);
            dispatch_async(myqueue, ^{
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    ChatImageViewController * imageChat=[storyboard instantiateViewControllerWithIdentifier:@"chatImage"];
                    NSString * str;
                    NSLog(@"Object %@",[resultArray objectAtIndex:indexPath.row]);
                    NSLog(@"CoreData %@",[self getImageFromCoreData:[resultArray objectAtIndex:indexPath.row]]);
                    
                    //for (Chat * info in [resultArray objectAtIndex:indexPath.row]) {
                    str=[self getImageFromCoreData:[resultArray objectAtIndex:indexPath.row]];
                    //}
                    imageChat.imageURL=str;
                    [self.navigationController pushViewController:imageChat animated:YES];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
            });
            
            
            
            
            
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}
@end
