//
//  MessagesInTellTeachers+CoreDataProperties.m
//  Tell123Teachers
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "MessagesInTellTeachers+CoreDataProperties.h"

@implementation MessagesInTellTeachers (CoreDataProperties)

+ (NSFetchRequest<MessagesInTellTeachers *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MessagesInTellTeachers"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic lastmsg;
@dynamic msgdetail;
@dynamic msgtype;

@end
