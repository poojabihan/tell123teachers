//
//  HistoryInTellTeachers+CoreDataProperties.h
//  Tell123Teachers
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryInTellTeachers+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface HistoryInTellTeachers (CoreDataProperties)

+ (NSFetchRequest<HistoryInTellTeachers *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSString *locationdetail;
@property (nullable, nonatomic, copy) NSString *msgdetail;

@end

NS_ASSUME_NONNULL_END
