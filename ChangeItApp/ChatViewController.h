//
//  ChatViewController.h
//  Tell123Teachers
//
//  Created by Tarun Sharma on 02/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import "Reachability.h"
#import "Webservice.h"

@interface ChatViewController : JSQMessagesViewController<JSQMessagesCollectionViewDataSource,JSQMessagesCollectionViewDelegateFlowLayout,UITextViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property NSString * itemID;
@property NSURLSessionConfiguration * sessionConfiguration;
@property NSURLSession * urlSession;
@property NSMutableURLRequest * request;
@property NSURLSessionDataTask * dataTask;
@property NSString * URLString;


@end
