//
//  HistoryTableViewCell.m
//  ChangeItApp
//
//  Created by Tarun Sharma on 26/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryTableViewCell.h"
#import "Reachability.h"
@implementation HistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.textOfLocation.adjustsFontSizeToFitWidth=YES;
    self.dateAndTime.adjustsFontSizeToFitWidth=YES;
    self.sendQueryButton.layer.cornerRadius = 8; // this value vary as per your desire
    
    self.sendQueryButton.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
